package ru.tsc.panteleev.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;

@RequestMapping("/api/tasks")
public interface TaskRestEndpoint {

    @PutMapping("/create")
    void create();

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findById/{id}")
    TaskDto findById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<TaskDto> findAll();

    @GetMapping("/count")
    long count();

    @PostMapping("/save")
    void save(@RequestBody TaskDto task);

    @PostMapping("/delete")
    void delete(@RequestBody TaskDto task);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<TaskDto> task);

    @DeleteMapping("/clear")
    void clear();

}


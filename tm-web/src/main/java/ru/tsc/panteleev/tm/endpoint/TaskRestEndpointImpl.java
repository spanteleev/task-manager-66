package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.api.TaskRestEndpoint;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.service.TaskDtoService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskDtoService taskDtoService;

    @Override
    @PutMapping("/create")
    public void create() {
        taskDtoService.create();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskDtoService.existsById(id);
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskDtoService.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() {
        return taskDtoService.findAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskDtoService.count();
    }

    @Override
    @PostMapping("/save")
    public void save(@NotNull @RequestBody final TaskDto task) {
        taskDtoService.save(task);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final TaskDto task) {
        taskDtoService.delete(task);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        taskDtoService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<TaskDto> tasks) {
        taskDtoService.deleteAll(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskDtoService.clear();
    }

}


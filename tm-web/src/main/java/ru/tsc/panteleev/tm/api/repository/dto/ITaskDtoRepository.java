package ru.tsc.panteleev.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

@Repository
public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

}

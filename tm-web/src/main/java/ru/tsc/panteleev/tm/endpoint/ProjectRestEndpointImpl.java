package ru.tsc.panteleev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.panteleev.tm.api.ProjectRestEndpoint;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.service.ProjectDtoService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectDtoService projectDtoService;

    @Override
    @PutMapping("/create")
    public void create() {
        projectDtoService.create();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectDtoService.existsById(id);
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectDtoService.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() {
        return projectDtoService.findAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectDtoService.count();
    }

    @Override
    @PostMapping("/save")
    public void save(@NotNull @RequestBody final ProjectDto project) {
        projectDtoService.save(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final ProjectDto project) {
        projectDtoService.delete(project);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectDtoService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<ProjectDto> projects) {
        projectDtoService.deleteAll(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectDtoService.clear();
    }

}


package ru.tsc.panteleev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import ru.tsc.panteleev.tm.listener.EntityListener;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectDto extends AbstractWbsDto {

    private static final long serialVersionUID = 1;

}

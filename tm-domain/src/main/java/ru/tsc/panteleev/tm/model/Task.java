package ru.tsc.panteleev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import ru.tsc.panteleev.tm.listener.EntityListener;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractWbs {

    private static final long serialVersionUID = 1;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}

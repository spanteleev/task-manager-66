package ru.tsc.panteleev.tm.dto.request.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class DataXmlJaxbSaveRequest extends AbstractUserRequest {

    public DataXmlJaxbSaveRequest(@Nullable String token) {
        super(token);
    }

}

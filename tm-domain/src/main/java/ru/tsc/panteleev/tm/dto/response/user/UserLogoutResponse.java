package ru.tsc.panteleev.tm.dto.response.user;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.panteleev.tm.dto.response.user.AbstractUserResponse;

@Getter
@Setter
public class UserLogoutResponse extends AbstractUserResponse {
}

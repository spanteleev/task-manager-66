package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.panteleev.tm.model.Task;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sortColumn")
    List<Task> findAllByUserIdSort(@Nullable @Param("userId") String userId,
                                   @Nullable @Param("sortColumn") String sortColumn);

    @Nullable
    Task findByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}

package ru.tsc.panteleev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.panteleev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.panteleev.tm.api.service.dto.ITaskDtoService;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.dto.model.SessionDto;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Date dateBegin = request.getDateBegin();
        @Nullable final Date dateEnd = request.getDateEnd();
        @Nullable TaskDto task = getTaskDtoService().create(userId, name, description, dateBegin, dateEnd);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<TaskDto> tasks = getTaskDtoService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDto task = getTaskDtoService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        getTaskDtoService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDto task = getTaskDtoService().changeStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getTaskDtoService().removeById(userId, id);
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDto task = getTaskDtoService().findById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDto task = getTaskDtoService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDto task = getTaskDtoService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowListByProjectIdResponse showListByProjectIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetListByProjectIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull final List<TaskDto> tasks = getTaskDtoService().findAllByProjectId(userId, projectId);
        return new TaskShowListByProjectIdResponse(tasks);
    }

}

package ru.tsc.panteleev.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.service.ISenderService;

import javax.jms.*;

public class SenderService implements ISenderService {

    @NotNull
    private static final String QUEUE_NAME = "TM_QUEUE";

    @NotNull
    private final BrokerService brokerService = new BrokerService();

    @NotNull
    private final ConnectionFactory factory = new ActiveMQConnectionFactory(
            ActiveMQConnectionFactory.DEFAULT_BROKER_URL
    );

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Queue destination;

    @NotNull
    private final MessageProducer producer;

    @SneakyThrows
    public SenderService() {
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
        connection = factory.createConnection();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE_NAME);
        producer = session.createProducer(destination);
    }

    @Override
    @SneakyThrows
    public void send(@NotNull final String message) {
        producer.send(session.createTextMessage(message));
    }

}

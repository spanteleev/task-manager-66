package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.dto.model.UserDto;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDto findById(@NotNull String id);

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

    void remove(@Nullable final UserDto user);

    void removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    UserDto setPassword(@Nullable String id, @Nullable String password);

    @Nullable
    UserDto updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
